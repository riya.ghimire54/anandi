<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    use HasFactory;
    
    
    protected $fillable =[
        'name',
        'image_featured',
        'description',
        'stock',
        'discount',
        'price',
        'tag',
        'size',
        'gender',
        'featured',
        'purity_id',
        'product_type_id',
        'category_id',
        
    ];

    public function images(){
        return $this->hasMany(Image::class);
    }

    public function occasions(){
        return $this->belongsToMany(Occasion::class);
    }

    public function purity(){
        return $this->belongsTo(Purity::class);
    }

    public function product_type(){
        return $this->belongsTo(ProductType::class);
    }

    public function jewellryTypes(){
        return $this->belongsToMany(JewellryType::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function orders(){
        return $this->hasMany(Order::class);
        }
}
