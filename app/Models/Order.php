<?php

namespace App\Models;

use Illuminate\Database\Console\Migrations\StatusCommand;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Order extends Model
{
    use HasFactory;
    protected $fillable=[
        'status',
        'user_id',
        'product_id',
        'item_purchased',
        'total_bill_amt',
        'shipping_address',
        'billing_address',
        'country',
        'zipcode',
        'quantity'
        ];
 public function user()
 {
     return $this->belongsTo(User::class);
 } 
public function products(){
    return $this->belongsTo(Product::class);
}         
}
