<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JewellryType extends Model
{
    use HasFactory;
    protected $fillable =[
        'jewellry_type',
        'description',
    ];

    public function products(){
        return $this->belongsToMany(Product::class);
    }

    public function purities(){
        return $this->belongsToMany(Purity::class);
    }


public function productTypes(){
    return $this->belongsToMany(ProductType::class);
}
}
