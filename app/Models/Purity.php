<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Purity extends Model
{
    use HasFactory;

    protected $fillable =[
        'purity_type',
        'description',
    ];

    public function products(){
        return $this->hasMany(Product::class);
    }

    public function jewellryTypes(){
        return $this->belongsToMany(JewellryType::class);
    }
}
