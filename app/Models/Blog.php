<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;
    protected $fillable = [
        'title',
        'btag_id',
        'image',
        'description',
        'slug',
        'excerpt',
        'isPublished',
        'publishedDate'
    ];
    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }

    public function btag()
    {
        return $this->belongsTo(btag::class);
    }
}
