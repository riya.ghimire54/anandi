<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;
    protected $fillable =[
       'business_name',
       'slogan',
       'image',
        'contact_info1',
        'contact_info2',
        'email1',
        'email2',
        'copyright_info',
        'delivery_details',
        'location_address',
        'privacy_policy',
        'term_condition',
        'facebook',
        'instagram',
        'twitter',
        'tiktok',
    ];
}
