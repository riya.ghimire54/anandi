<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\Testimonial\TestimonialCollection;
use App\Http\Resources\Testimonial\TestimonialResource;
use App\Models\Testimonial;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new TestimonialCollection(Testimonial::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required|unique:testimonials,name',
            'designation' =>'required',
            'description' =>'required',
            'image'=>'nullable|image|mimes:png,jpeg,jpg,webp,svg',
        ]);
        $input = $request->all();

        if ($request->hasFile('image')) {
            $profileImage = str_slug(request()->name) . "." . request()->image->getClientOriginalExtension();
            request()->image->move(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'testimonials', $profileImage);
            $input['image'] = $profileImage;
        }
        Testimonial::create($input);
        return response()->json([
            'success' => 'saved!',
            "testimonials" => new TestimonialResource($input)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new TestimonialResource(Testimonial::FindOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|unique:testimonials,name,'.$id,
            'designation'=>'required',
            'description'=>'required',
            'image'=>'nullable|image|mimes:png,jpeg,jpg,webp,svg',

        ]);
        $input = $request->all();

        $testimonial = Testimonial::findOrFail($id);
        if ($request->hasFile('image')) {
            $profileImage = str_slug(request()->name) . "." . request()->image->getClientOriginalExtension();
            if (file_exists(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'testimonials'.DIRECTORY_SEPARATOR.$testimonial->image)) {
                unlink(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'testimonials'.DIRECTORY_SEPARATOR.$testimonial->image);
            }
            request()->image->move(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'testimonials', $profileImage);
            $input['image'] = $profileImage;
        }

        $testimonial->name = $request->get('name');
        $testimonial->designation = $request->get('designation');
        $testimonial->description =$request->get('description');
        if ($request->has('image')) {
            $testimonial->image = $input['image'];
        }
        $testimonial->save();
        return response()->json([
            'success' => 'testimonials updated!',
            "testimonial" => new TestimonialResource($testimonial)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $testimonial = Testimonial::findOrFail($id);

        if (file_exists(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'testimonials'.DIRECTORY_SEPARATOR.$testimonial->image)) {
            unlink(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'testimonials'.DIRECTORY_SEPARATOR.$testimonial->image);
        }

        $testimonial->delete();

        return response()->json(['danger' => 'Removed.']);
    }
}
