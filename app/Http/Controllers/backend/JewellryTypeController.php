<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\JewellryType\JewellryTypeCollection;
use App\Http\Resources\JewellryType\JewellryTypeResource;
use App\Http\Resources\Purity\PurityCollection;
use App\Models\JewellryType;
use App\Models\Purity;
use Illuminate\Http\Request;

class JewellryTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $jewellery_types = JewellryType::all();
        // foreach ($jewellery_types as $jewellery_type) {
        //     dd($jewellery_type->pivot);
        //     foreach ($jewellery_type->purities as $key => $purity) {
        //         $jewellery_type["purities"] = $purity->pluck('purity_type');
        //     }
        // }
        return new JewellryTypeCollection(JewellryType::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'jewellry_type'=>'required|unique:jewellry_types,jewellry_type',
        ]);

       $jewellryTypes = JewellryType::create($request->all());

        if (isset($request->purity)) {
            foreach($request->purity as $id){
                $purity = Purity::find($id);
                $jewellryTypes->purities()->attach($purity);
            }
        }

        return response()->json([
            'success' => 'saved!',
            "jewellery_type" => new JewellryTypeResource($jewellryTypes)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new JewellryTypeResource(JewellryType::FindOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'jewellry_type'=>'required|unique:jewellry_types,jewellry_type,'.$id,
        ]);

        $jewellry = JewellryType::findOrFail($id);
        $jewellry->jewellry_type = $request->get('jewellry_type');
        $jewellry->description = $request->get('description');

        if (isset($request->purity)) {
            foreach($request->purity as $id){
                $purity = Purity::find($id);
                $jewellry->purities()->syncWithoutDetaching([$purity->id]);
            }
        }
        $jewellry->save();

        return response()->json(['success' => 'updated!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jewellry = JewellryType::findOrFail($id);

        $jewellry->purities()->detach();
        $jewellry->delete();

        return response()->json(['danger' => 'Removed']);
    }
}
