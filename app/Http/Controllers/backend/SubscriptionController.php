<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\Subscription\SubscriptionCollection;
use App\Http\Resources\Subscription\SubscriptionResource;
use App\Http\Resources\Subscription\SubscriptipnResource;
use App\Models\Subscription;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    public function index(){
      return new SubscriptionCollection(Subscription::paginate(10));
    }
    
    public function store(Request $request){
        $request->validate([
            'email'=>['required','email','unique:subscriptions,email'],
        ]);
        $input = $request->all();
    
    
           $subscription = Subscription::create($input);
           return response()->json(['success' => 'Sucessfully subscried!',
         
        ]);
    }
}
