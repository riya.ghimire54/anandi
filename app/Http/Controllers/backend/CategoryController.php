<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\Category\CategoryCollection;
use App\Http\Resources\Category\CategoryResource;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new CategoryCollection(Category::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'category_name'=>'required|unique:categories,category_name',
            'category_description'=>'required',
            'image'=>'nullable|image|mimes:png,jpeg,jpg,webp,svg',
        ]);
        $input = $request->all();

        if ($request->hasFile('image')) {
            $profileImage = str_slug(request()->category_name) . "." . request()->image->getClientOriginalExtension();
            request()->image->move(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'categories', $profileImage);
            $input['image'] = $profileImage;
        }
        $category = Category::create($input);
        return response()->json([
            'success' => 'category saved!',
            'category' => new CategoryResource($category)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new CategoryResource(Category::findOrFail($id));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->id);
        $request->validate([
            'category_name'=>'required|unique:categories,category_name,'.$id,
            'category_description'=>'required',
            'image'=>'nullable|image|mimes:png,jpeg,jpg,webp,svg',
        ]);
        $category = Category::findOrFail($id);
        $input = $request->all();

        if ($request->hasFile('image')) {
            $profileImage = str_slug(request()->category_name) . "." . request()->image->getClientOriginalExtension();
            if (file_exists(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'categories'.DIRECTORY_SEPARATOR.$category->image)) {
                unlink(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'categories'.DIRECTORY_SEPARATOR.$category->image);
            }
            request()->image->move(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'categories', $profileImage);
            $input['image'] = $profileImage;
        }

        $category->category_name = $request->get('category_name');
        $category->category_description =$request->get('category_description');
        if ($request->has('image')) {
            $category->image = $input['image'];
        }
        $category->save();
        return response()->json([
            'success' => 'category updated!',
            'category' => new CategoryResource($category)
        ]);

    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        if (file_exists(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'categories'.DIRECTORY_SEPARATOR.$category->image)) {
            unlink(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'categories'.DIRECTORY_SEPARATOR.$category->image);
        }
        $category->delete();

        return response()->json(['danger' => 'Removed.']);
    }
}

