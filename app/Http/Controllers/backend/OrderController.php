<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\Order\OrderCollection;
use App\Http\Resources\Order\OrderResource;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new OrderCollection(Order::paginate(8));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'status'=>'required|unique:orders,status',
            
            'item_purchased'=>'required',
            'total_bill_amt'=>'required',
            'shipping_address'=>'required',
            'billing_address'=>'required',
            'country'=>'required',
            'zipcode'=> 'required',
            'quantity'=>'required',




        ]);
           $input= $request->all();
        //    dd($input);
           $input['user_id']= Auth::user()->id;
        
           $order = Order::create($input);
        return response()->json([
            'success' => 'saved!',
            "order" => new OrderResource ($order)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    


    public function show($id)
    {
        return new OrderResource(Order::FindOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'status'=>'required|unique:orders,status,'.$id,
            'user_id' =>'required|unique:orders,user_id,'.$id,
            'item_purchased'=>'required',
            'total_bill_amt'=>'required',
            'shipping_address'=>'required',
            'billing_address'=>'required',
            'country'=>'required',
            'zipcode'=>'required',
            'quantity'=>'required',
        ]);

        $order = Order::findOrFail($id);
        $order->status = $request->get('status');
        $order->item_purchased = $request->get('iteam_purchased');
        $order->total_bill_amount =$request->get('total_bill_amt');
        $order->shipping_address =$request->get('shipping_address');
        $order->billing_address =$request->get('billing_address');
        $order->country=$request->get('country');
        $order->zipcode=$request->get('zipcode');
        $order->quantity=$request->get('quantity');
    
        $order->save();
        return response()->json([
            'success' => 'updated!',
            'order' => new OrderResource($order)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::findOrFail($id);
        $order->delete();

        return response()->json(['danger' => 'Removed']);
    }
}
