<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\Product\ProductCollection;
use App\Http\Resources\Product\ProductResource;
use App\Models\Image;
use App\Models\JewellryType;
use App\Models\Occasion;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Client\ResponseSequence;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ProductCollection(Product::paginate(8));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function image(Request $request)
    {
        if (!$request->hasFile('fileName')) {
            return response()->json(['upload_file_not_found'], 400);
        }

        $allowedfileExtension=['pdf','jpg','png'];
        $files = $request->file('fileName');
        $errors = [];
        $product = Product::findOrFail($request->product_id);
        foreach ($files as $file) {
            $extension = $file->getClientOriginalExtension();

            $check = in_array($extension, $allowedfileExtension);

            if ($check) {
                foreach ($request->fileName as $key => $mediaFiles) {
                     if  ($key < 4){
                    $name = $product->name."_".$key.".".$mediaFiles->getClientOriginalExtension();
                    $path = public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'products'.DIRECTORY_SEPARATOR.$product->name;
                    $mediaFiles->move($path, $name);
                    //store image file into directory and db
                    $save = new Image();
                    $save->title = $name;
                    $save->product_id = $request->product_id;
                    $save->path = env("APP_URL")."/".$path;
                    $save->save();
                }
                else{
                    return response()->json(['Only 4 files uploaded']);
                }
            
                }
            } 
            else {
                return response()->json(['invalid_file_format'], 422);
            }

            return response()->json(['file_uploaded'], 200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required|unique:products,name',
            'stock' =>'required',
            'description' =>'required',
            'discount' =>'nullable',
            'price' =>'required',
            'tag'=>'required',
            'gender'=>'required',
            'image_featured'=>'nullable|image|mimes:png,jpeg,jpg,webp,svg',
            
        ]);

        $input = $request->all();

        if ($request->hasFile('image_featured')) {
            $profileImage = str_slug(request()->name) . "." . request()->image_featured->getClientOriginalExtension();
            request()->image_featured->move(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'products'.DIRECTORY_SEPARATOR.$request->name, $profileImage);
            $input['image_featured'] = $profileImage;
        }

        $product = Product::create($input);

        if (isset($request->occasion)) {
            foreach ($request->occasion as $id) {
                $occasion = Occasion::find($id);
                $product->occasions()->attach($occasion);
            }
        }

        if (isset($request->jewellryType)) {
            foreach ($request->jewellryType as $id) {
                $jewellryType =JewellryType::find($id);
                $product->jewellryTypes()->attach($jewellryType);
            }
        }

        return response()->json([
            'success' => 'saved!',
            "product" => new ProductResource($product)
        ]);
    }
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        $product['images'] = Image::where('product_id', $id)->pluck('path');
        // dd($images);
        // foreach ($images as $image) {
        //     $product['images'] = env("APP_URL").$image;
        // }
        return new ProductResource($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|unique:products,name,'.$id,
            'stock'=>'required',
            'discount'=>'nullable',
            'price'=>'required',
            'tag'=>'required',
            'gender'=>'required',
            'size'=>'nullable',
            'description'=>'required',
            'image_featured'=>'nullable|image|mimes:png,jpeg,jpg,webp,svg',
        ]);

        $product = Product::findOrFail($id);
        $input = $request->all();

        if ($request->hasFile(['image_featured'])) {
            $profileImage = str_slug(request()->name) . "." . request()->image->getClientOriginalExtension();
            if (file_exists(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'products'.DIRECTORY_SEPARATOR.$request->name.DIRECTORY_SEPARATOR.$product->image_featured)) {
                unlink(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'products'.DIRECTORY_SEPARATOR.$request->name.DIRECTORY_SEPARATOR.$product->image_featured);
            }
            request()->image->move(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'products'.DIRECTORY_SEPARATOR.$request->name, $profileImage);
            $input['image_featured'] = $profileImage;
        }

        if (isset($request->occasions)) {
            foreach ($request->occasions as $id) {
                $occasion = Occasion::find($id);
                $product->occasions()->syncWithoutDetaching([$occasion->id]);
            }
        }

        if (isset($request->jewellryType)) {
            foreach ($request->jewellryType as $id) {
                $jewellryType =JewellryType::find($id);
                $product->jewellryTypes()->syncWithoutDetaching($jewellryType);
            }
        }

        $product->update($input);
        return response()->json([
            'success' => 'updated!',
            "product" => new ProductResource($product)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);

        if (file_exists(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'products'.DIRECTORY_SEPARATOR.$product->name.DIRECTORY_SEPARATOR.$product->image)) {
            unlink(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'products'.DIRECTORY_SEPARATOR.$product->name.DIRECTORY_SEPARATOR.$product->image);
        }

        if (isset($product->occasion)) {
            $occasion = Occasion::find($product->occasion->id);
            $product->Occasion()->detach($occasion);
        }

        if (isset($product->jewellery_type)) {
            $jewellry_type = JewellryType::find($product->jewellry_type->id);
            $product->Jewellry_types()->detach($jewellry_type);
        }

        $product->delete();
        return response()->json(['danger' => 'Removed.']);
    }

    
}
