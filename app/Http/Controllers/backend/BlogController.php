<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\Blog\BlogCollection;
use App\Http\Resources\Blog\BlogResource;
use App\Models\Blog;
use App\Models\Btag;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new BlogCollection(Blog::paginate(8));
    }

    /**
      * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255|unique:blogs,title',
            'image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description'=>'required',
            'btag_id' => 'required',
            "excerpt" => "nullable|max:255",
            "isPublished" => "required|boolean",
            "publishedDate" => "date"
        ]);

        $input = $request->all();

        if ($request->hasFile('image')) {
            $profileImage = str_slug(request()->title) . "." . request()->image->getClientOriginalExtension();
            request()->image->move(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'blogs', $profileImage);
            $input['image'] = $profileImage;
        }
        if ($request['isPublished'] == 1){
            $input['publishedDate'] = Carbon::today();
        }

        $blog = Blog::create($input);
        return response()->json([
            'success' => 'Blog saved!',
            'blog' => new BlogResource($blog)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new BlogResource(Blog::FindOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|max:255|unique:blogs,title,'.$id,
            'image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description'=>'required',
            'btag_id' => 'required',
            "excerpt" => "nullable|max:255",
            "isPublished" => "required|boolean",
            "publishedDate" => "date"
        ]);
        $blog = Blog::findOrFail($id);
        $input = $request->all();

        if ($request->hasFile('image')) {
            $profileImage = str_slug(request()->title) . "." . request()->image->getClientOriginalExtension();
            if (file_exists(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'blogs'.DIRECTORY_SEPARATOR.$blog->image)) {
                unlink(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'blogs'.DIRECTORY_SEPARATOR.$blog->image);
            }
            request()->image->move(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'blogs', $profileImage);
            $input['image'] = $profileImage;
        }

        $blog->title =  $request->get('title');
        $blog->image = $input['image'];
        $blog->description = $request->get('description');
        $blog->excerpt= $request->get('excerpt');
        $blog->slug = str_slug($request->title);
        $blog->isPublished = $request->get('isPublished');
        if ($request['isPublished'] == 1){
            $blog->publishedDate = Carbon::today();
        }

        $blog->save();
        return response()->json([
            'success' => 'Blog update!',
            "blog" => new BlogResource($blog)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::findOrFail($id);
        if (file_exists(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'blogs'.DIRECTORY_SEPARATOR.$blog->image)) {
            unlink(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'blogs'.DIRECTORY_SEPARATOR.$blog->image);
        }
        $blog->delete();
        return response()->json(['danger' => 'Removed.']);
    }
}
