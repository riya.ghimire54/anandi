<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\Ocassion\OcassionCollection;
use App\Http\Resources\Ocassion\OcassionResource;
use App\Models\Occasion;
use Illuminate\Http\Request;

class OccasionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new OcassionCollection(Occasion::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'occasion_name'=>'required|unique:occasions,occasion_name',
        ]);

        $occasion = Occasion::create($request->all());

        return response()->json([
            'success' => 'admin saved!',
            "occasion" => new OcassionResource($occasion)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new OcassionResource(Occasion::FindOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'occasion_name'=>'required|unique:occasions,occasion_name,'.$id,
        ]);

        $occasion = Occasion::findOrFail($id);
        $occasion->occasion_name = $request->get('occasion_name');
        $occasion->description = $request->get('description');
        $occasion->save();

        return response()->json([
            'success' => ' updated!',
            "occasion" => new OcassionResource($occasion)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $occasion = Occasion::findOrFail($id);
        $occasion->delete();

        return response()->json(['danger' => 'Removed']);
    }
}
