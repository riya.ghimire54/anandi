<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\OurTeam\OurTeamCollection;
use App\Http\Resources\OurTeam\OurTeamResource;
use App\Models\OurTeam;
use Illuminate\Http\Request;

class OurTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new OurTeamCollection(OurTeam::paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'designation' => 'required',
            'facebook'=>'nullable',
            'instagram'=>'nullable',
            'linkedin'=>'nullable',
            'twitter'=>'nullable',
            'image' => 'nullable|image|mimes:png,jpeg,jpg,webp,svg',
            'email'=>'required','email','unique:our_teams,email',
        ]);
        $input = $request->all();
         
         //saving a file
         if ($request->hasFile('image')) {
            $profileImage = "ourteam-image" . "." . request()->image->getClientOriginalExtension();
            request()->image->move(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'ourteam', $profileImage);
            $input['image'] = $profileImage;
        }

        // creating an entry on database
        $ourteam = OurTeam::create($input);

        // returning a response
        return response()->json([
            'success' => ' saved!',
            "ourteam" => new OurTeamResource($ourteam)
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new OurTeamResource(OurTeam::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $request->validate([
            'name'=>'required',
            'designation'=>'required',
            'facebook'=>'nullable',
            'twitter'=>'nullable',
            'linkedin'=>'nullable',
            'instagram'=>'nullable',
           'image_featured'=>'nullable|image|mimes:png,jpeg,jpg,webp,svg',
           'email'=>'required','email','unique:our_teams,email',
        ]);

        $ourteam = OurTeam::findOrFail($id);
        $input = $request->all();

        if ($request->hasFile(['image_featured'])) {
            $profileImage = str_slug(request()->name) . "." . request()->image->getClientOriginalExtension();
            if (file_exists(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'our_teams'.DIRECTORY_SEPARATOR.$request->name.DIRECTORY_SEPARATOR.$ourteam->image)) {
                unlink(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'our_teams'.DIRECTORY_SEPARATOR.$request->name.DIRECTORY_SEPARATOR.$ourteam->image);
            }
            request()->image->move(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'our_teams'.DIRECTORY_SEPARATOR.$request->name, $profileImage);
            $input['image'] = $profileImage;
        }
        $ourteam->update($input);
        return response()->json([
            'success' => 'updated!',
            "ourteam" => new OurTeamResource($ourteam)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ourteam = OurTeam::findOrFail($id);

        if (file_exists(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'ourteam'.DIRECTORY_SEPARATOR.$ourteam->image)) {
            unlink(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'ourteam'.DIRECTORY_SEPARATOR.$ourteam->image);
        }

        $ourteam->delete();

        return response()->json(['danger' => 'Removed.']);
    }
}
