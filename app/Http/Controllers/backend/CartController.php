<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\Cart\CartCollection;
use App\Models\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function index (){
        $cart =Cart::where('user_id',Auth::id())->get();
        return new CartCollection($cart);
    }

    public function store (Request $request){
        if (Auth::check())
            $product_id = $request->input('product_id');
            if(cart::where('product_id', $product_id)->where('user_id', Auth::id())->exists())
            {
                $cart= Cart::where ('product_id', $product_id)->where('user_id', Auth::id())->first();
                $cart->delete();
                return response()->json(["message"=> "remove from cart"]);
            }
            else {
                $cart = new Cart();
                $cart->product_id =$product_id;
                $cart->user_id=Auth::id();
                $cart->save();

                return response()->json([
                    "message" => "added to cartlist"
             ]);
            }
}
}