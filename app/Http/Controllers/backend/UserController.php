<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\UserCollection;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function profile() 
    {
        $profile = auth()->user();
        return new UserResource([
            'title'=>'User Profile',
            'data' =>$profile,
        ]);
    }
public function index ()
    {
        return new UserCollection(User::paginate(10));
    }
public function update(Request $request,$id)
{
   
        $request->validate([
            'title'=>['required'],
            'name' => ['required', 'string', 'max:255'],
            'contact_number'=>['required','regex:/^([0-9\s\-\+\(\)]*)$/', 'min:10', 'max:15'],
            'address'=>['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email'],
            'image'=>['nullable','image','mimes:png,jpeg,jpg,webp,svg'],
    ]);
    
       $user = User::findOrFail($id);
        $input = $request->all();
    
        if ($request->hasFile(['image'])) {
        $profileImage = str_slug(request()->email) . "." . request()->image->getClientOriginalExtension();
        if (file_exists(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'user'.DIRECTORY_SEPARATOR.$request->email.DIRECTORY_SEPARATOR.$user->image)) {
            unlink(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'user'.DIRECTORY_SEPARATOR.$request->email.DIRECTORY_SEPARATOR.$user->image);
        }
        request()->image->move(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'products'.DIRECTORY_SEPARATOR.$request->email, $profileImage);
        $input['image'] = $profileImage;
     
}    
 $user->update($input);
 return response()->json(['updated']);   
}

}