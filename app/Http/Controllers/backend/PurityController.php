<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\Purity\PurityCollection;
use App\Http\Resources\Purity\PurityResource;
use App\Models\Purity;
use Illuminate\Http\Request;

class PurityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       return new PurityCollection(Purity::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'purity_type'=>'required|unique:purities,purity_type'
        ]);

        $purity = Purity::create($request->all());
        return response()->json([
            'success' => 'saved!',
            "purity" => new PurityResource($purity)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new PurityResource(Purity::FindOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'purity_type'=>'required|unique:purities,purity_type,'.$id,
        ]);

        $purity = Purity::findOrFail($id);
        $purity->purity_type = $request->get('purity_type');
        $purity->description = $request->get('description');
        $purity->save();

        return response()->json([
            'success' => 'updated!',
            "purity" => new PurityResource($purity)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $purity = Purity::findOrFail($id);
        $purity->delete();
        return response()->json(['danger' => 'Removed']);
    }
}
