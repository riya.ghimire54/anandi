<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\About\AboutCollection;
use App\Http\Resources\About\AboutResource;
use App\Models\About;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //sending all the data in database
        return new AboutCollection(About::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validating
        $request->validate([
            'title' => 'required|unique:abouts,title',
            'description' => 'required',
            'image' => 'nullable|image|mimes:png,jpeg,jpg,webp,svg'
        ]);

        $input = $request->all();

        //saving a file
        if ($request->hasFile('image')) {
            $profileImage = "about-image" . "." . request()->image->getClientOriginalExtension();
            request()->image->move(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'about', $profileImage);
            $input['image'] = $profileImage;
        }

        // creating an entry on database
        $about = About::create($input);

        // returning a response
        return response()->json([
            'success' => 'About saved!',
            "about" => new AboutResource($about)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new AboutResource(About::findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'=>'required|unique:abouts,title,'.$id,
            'description'=>'required',
            'image'=>'nullable|image|mimes:png,jpeg,jpg,webp,svg',

        ]);
        $about = About::findOrFail($id);

        $input = $request->all();

        if ($request->hasFile('image')) {
            $profileImage = "about-image-updated" . "." . request()->image->getClientOriginalExtension();
            if (file_exists(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'about'.DIRECTORY_SEPARATOR.$about->image)) {
                unlink(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'about'.DIRECTORY_SEPARATOR.$about->image);
            }
            request()->image->move(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'about', $profileImage);
            $input['image'] = $profileImage;
        }
        $about->title = $request->get('title');
        $about->description =$request->get('description');
        if ($request->hasFile('image')) {
            $about->image = $input['image'];
        }
        $about->save();

        return response()->json([
            'success' => 'updated!',
            'about' => new AboutResource($about)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $about = About::findOrFail($id);

        if (file_exists(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'about'.DIRECTORY_SEPARATOR.$about->image)) {
            unlink(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'about'.DIRECTORY_SEPARATOR.$about->image);
        }

        $about->delete();

        return response()->json(['danger' => 'Removed.']);
    }
}
