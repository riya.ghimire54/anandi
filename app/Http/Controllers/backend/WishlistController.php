<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\Wishlist\WishlistCollection;
use App\Models\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishlistController extends Controller
{  
    public function index (){
        $wishlist =Wishlist::where('user_id',Auth::id())->get();
        return new WishlistCollection($wishlist);
    }

    public function store (Request $request){
        if (Auth::check())
            $product_id = $request->input('product_id');
            if(Wishlist::where('product_id', $product_id)->where('user_id', Auth::id())->exists())
            {
                $wishlist= Wishlist::where ('product_id', $product_id)->where('user_id', Auth::id())->first();
                $wishlist->delete();
                return response()->json(["message"=> "unwish"]);
            }
            else {
                $wish = new Wishlist();
                $wish->product_id =$product_id;
                $wish->user_id=Auth::id();
                $wish->save();

                return response()->json([
                    "message" => "added to wishlist"
                ]);
            }
    }
}
