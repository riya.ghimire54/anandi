<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\Setting\SettingCollection;
use App\Http\Resources\Setting\SettingResource;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new SettingCollection(Setting::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new SettingResource(Setting::findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'business_name'=>'required|unique:settings,business_name',
            'slogan'=>'required',
            'image'=>'nullable|image|mimes:png,jpeg,jpg,webp,svg',
            'location_address'=>'required',
            'contact_info1'=>'required','regex:/^([0-9\s\-\+\(\)]*)$/', 'min:10', 'max:15',
            'contact_info2'=>'nullable',
             'email1'=>'required', 'string', 'email', 'max:255',
             'email2'=>'nullable',
             'copyright_info'=>'required',
             'delivery_details'=>'required',
             'privacy_policy'=>'required',
             'term_condition'=>'required',
             'facebook'=>'nullable',
             'instagram'=>'nullable',
             'twitter'=>'nullable',
             'tiktok'=>'nullable'


        ]);
        //  $setting = Setting::first();
         $input = $request->all();
        // dd(Setting::first());
        if ($request->hasFile('image')) {
            $profileImage = "setting-image-updated" . "." . request()->image->getClientOriginalExtension();
            if (file_exists(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'setting'.DIRECTORY_SEPARATOR.$request->image)) {
                unlink(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'setting'.DIRECTORY_SEPARATOR.$request->image);
            }
            request()->image->move(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'setting', $profileImage);
            $input['image'] = $profileImage;
        }
        
        // $setting->business_name = $request->get('business_name');
        // $setting->slogan =$request->get('slogon');
        // $setting->location_address =$request->get('location_address');
        // $setting->contact_info1= $request->get('contact_info1');
        // $setting->contact_info2 =$request->get('contact_info2');
        // $setting->email1=$request->get('email1');
        // $setting->email2 =$request->get('email2');
        // $setting->copyright_info =$request->get('copyright_info');
        // $setting->delivery_details= $request->get('delivery_details');
        
        // if ($request->hasFile('image')) {
        //     $setting->image = $input['image'];
        // }
        $setting = Setting::firstOrCreate($input);

        return response()->json([
            'success' => 'updated!',
            'setting' => new SettingResource($setting)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $setting = Setting::findOrFail($id);

        if (file_exists(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'setting'.DIRECTORY_SEPARATOR.$setting->image)) {
            unlink(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'setting'.DIRECTORY_SEPARATOR.$setting->image);
        }

        $setting->delete();

        return response()->json(['danger' => 'Removed.']);
    }
}
