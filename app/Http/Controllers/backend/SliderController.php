<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\Slider\SliderCollection;
use App\Http\Resources\Slider\SliderResource;
use App\Models\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new SliderCollection(Slider::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           'image' => 'required|image|mimes:png,jpeg,jpg,webp,svg'
        ]);
        $input = $request->all();

      
        if ($request->hasFile('image')) {
            $profileImage = "slider-image" . "." . request()->image->getClientOriginalExtension();
            request()->image->move(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'slider', $profileImage);
            $input['image'] = $profileImage;
        }

        $slider = Slider::create($input);

        return response()->json([
            'success' => 'image saved!',
            "about" => new SliderResource($slider)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new SliderCollection(Slider::findOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
          'image'=>'required|image|mimes:png,jpeg,jpg,webp,svg',

        ]);
        $slider = Slider::findOrFail($id);

        $input = $request->all();

        if ($request->hasFile('image')) {
            $profileImage = "slider-image-updated" . "." . request()->image->getClientOriginalExtension();
            if (file_exists(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'slider'.DIRECTORY_SEPARATOR.$slider->image)) {
                unlink(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'slider'.DIRECTORY_SEPARATOR.$slider->image);
            }
            request()->image->move(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'slider', $profileImage);
            $input['image'] = $profileImage;
        }
       
        if ($request->hasFile('image')) {
            $slider->image = $input['image'];
        }
        $slider->save();

        return response()->json([
            'success' => ' Slider image updated!',
            'slider' => new SliderResource($slider)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Slider::findOrFail($id);

        if (file_exists(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'slider'.DIRECTORY_SEPARATOR.$slider->image)) {
            unlink(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'slider'.DIRECTORY_SEPARATOR.$slider->image);
        }

        $slider->delete();

        return response()->json(['danger' => 'Removed.']);
    }
}
