<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductType\ProductTypeCollection;
use App\Http\Resources\ProductType\ProductTypeResource;
use App\Models\JewellryType;
use App\Models\ProductType;
use Illuminate\Http\Request;

class ProductTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ProductTypeCollection(ProductType::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required|unique:product_types,name',
            'description'=>'required',
            'image'=>'nullable|image|mimes:png,jpeg,jpg,webp,svg',
        ]);

        $input = $request->all();

        if ($request->hasFile('image')) {
            $profileImage = str_slug(request()->name) . "." . request()->image->getClientOriginalExtension();
            request()->image->move(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'product_type'.DIRECTORY_SEPARATOR.$request->name, $profileImage);
            $input['image'] = $profileImage;
        }
       $producttype = ProductType::create($request->all());
        if (isset($request->jewellryType)) {
            foreach($request->jewellryType as $id){
                $jewellryType =JewellryType::find($id);
                $producttype->jewellryTypes()->attach($jewellryType);
            }
        }
        return response()->json(['success' => 'Product saved!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new ProductTypeResource(ProductType::FindOrFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|unique:product_types,name,'.$id,
            'description'=>'required',
            'image'=>'nullable|image|mimes:png,jpeg,jpg,webp,svg',

        ]);
        $input = $request->all();

        $producttype = ProductType::findOrFail($id);

        if ($request->hasFile('image')) {
            $profileImage = str_slug(request()->name) . "." . request()->image->getClientOriginalExtension();
            if (file_exists(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'product_type'.DIRECTORY_SEPARATOR.$producttype->image)) {
                unlink(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'product_type'.$producttype->image);
            }
            request()->image->move(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'product_type', $profileImage);
            $input['image'] = $profileImage;
        }

        $producttype->name = $request->get('name');
        $producttype->description =$request->get('description');
        if ($request->has('image')) {
            $destinationPath = 'image/';
            $profileImage = date('YmdHis') . "." . $request['image']->getClientOriginalExtension();
            $request['image']->move($destinationPath, $profileImage);
            $input['image'] = "$profileImage"; 
    }
       
    $producttype = ProductType::find($id);
    $producttype->name = $request->get('name');
    $producttype->description =$request->get('description');
    if ($request->has('image')) {
        $producttype->image = $input['image'];
    }

    if (isset($request->jewellryType)) {
        foreach($request->jewellryType as $id){
            $jewellryType =JewellryType::find($id);
            $producttype->jewellryTypes()->attach($jewellryType);
        }
    }
    $producttype->save();
    return response()->json(['success' => 'Product type updated!']); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producttype = ProductType::findOrFail($id);
        if (file_exists(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'product_type'.DIRECTORY_SEPARATOR.$producttype->image)) {
            unlink(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'product_type'.DIRECTORY_SEPARATOR.$producttype->image);
        }
        $producttype->delete();

        return response()->json(['danger' => 'Removed.']);
    }
}
