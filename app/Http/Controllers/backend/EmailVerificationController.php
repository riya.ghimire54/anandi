<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;

class EmailVerificationController extends Controller
{
    public function __construct()
    
      {
            $this->middleware('auth:api')->only('resend');
            $this->middleware('signed')->only('verify');
            $this->middleware('throttle:6,1')->only('verify', 'resend');
        }
        public function verify(Request $request)
        {
            if (!$request->hasValidSignature()) {
                return response()->json(["msg" => "Invalid/Expired url provided."], 401);
            }
    
            $user = User::find($request->route('id'));
    
            if ($user->hasVerifiedEmail()) {
                return response()->json(['message' => 'This email is already verified']);
            }
    
            if ($user->markEmailAsVerified()) {
                event(new Verified($request->user()));
            }
    
            return response()->json(['message' => 'You have been successfully verified']);
        }
        public function resend(Request $request)
    {
        if ($request->user()->hasVerifiedEmail()) {
            return response()->json(['message' => 'Email already verified'], 400);
        }

        $request->user()->sendEmailVerificationNotification();

        if ($request->wantsJson()) {
            return response()->json(['success' => 'A verification email has been sent. Please make sure you are verified to complete your verification process']);
        }

        return back()->with('resent', true);
    }
}
