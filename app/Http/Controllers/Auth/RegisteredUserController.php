<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class RegisteredUserController extends Controller
{
    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        // dd('sdkfd');
        $request->validate([
            'title'=>['required'],
            'name' => ['required', 'string', 'max:255'],
            'contact_number'=>['required','regex:/^([0-9\s\-\+\(\)]*)$/', 'min:10', 'max:15'],
            'address'=>['required'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
            'image'=>['nullable','image','mimes:png,jpeg,jpg,webp,svg'],

        ]);
        $input=$request->all();
         
        if ($request->hasFile('image')) {
            $profileImage = str_slug(request()->email) . "." . request()->image->getClientOriginalExtension();
            request()->image->move(public_path().DIRECTORY_SEPARATOR.'image'.DIRECTORY_SEPARATOR.'user'.DIRECTORY_SEPARATOR.$request->email, $profileImage);
            $input['image'] = $profileImage;
        }

        $user = User::create([
            'name' => $request->name,
            'title' => $request->title,
            'contact_number'=>$request->contact_number,
            'address'=>$request->address,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'image'=>$input['image'],
 ]);

        
        $user->sendEmailVerificationNotification();
        event(new Registered($user));

        Auth::login($user); 

        return response()->noContent();
        // return response()->json(['message' => "user has been registererd"]);

    }
}
