<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticatedSessionController extends Controller
{
    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LoginRequest $request)
    {
        // dd($request);
        $request->authenticate();
        $token =Auth::user()->createToken('login')->plainTextToken;



        $request->session()->regenerate();
        // if(Auth::user()->hasRole('superadministrator'))
        // return response()->json("Superadmin");
        
        // else if(Auth::user()->hasRole('administrator'))
        // return response()->json('Admin');

        // else if (Auth::user()->hasRole('user'))
        // return response()->json('user');

        // else 
        // return response()->json('no role assign');

        return response()->json([
            'message' => 'Login successful',
            'user' => Auth::user(),
            'access_token' => $token
        ]);

    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return response()->noContent();
    }
}
