<?php

namespace App\Http\Resources\Category;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data =[
            'id'=>$this->id,
            'category_name'=>$this->category_name,
            'category_description'=>$this->category_description,
            'created_at'=> $this->created_at,
            'updated_at'=>$this->updated_at,
            'image' =>  isset($this->image)? env("APP_URL")."/image/categories/".$this->category_name."/".$this->image:"",
            ];
        
        return $data; 
    }
}
