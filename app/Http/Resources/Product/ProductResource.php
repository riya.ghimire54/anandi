<?php

namespace App\Http\Resources\Product;

use App\Http\Resources\Category\CategoryResource;
use App\Http\Resources\ProductType\ProductTypeResource;
use App\Http\Resources\Purity\PurityResource;
use App\Models\Category;
use App\Models\ProductType;
use App\Models\Purity;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
       $data =[
           'id'=>$this->id,
           'name'=>$this->name,
           'image_featured'=>isset($this->image_featured)? env("APP_URL")."/image/products/".$this->name."/".$this->image_featured:"",
            'description'=>$this->description,
            'stock'=>$this->stock,
            'discount'=>isset($this->discount)?$this->discount:"",
            'price'=>$this->price,
            'tag'=>$this->tag,
            'size'=>isset($this->size)?$this->size:"",
            'gender'=>$this->gender,
            'purity'=>$this->purity->purity_type,
            'product_type' => $this->product_type->name,
            'category' => $this->category->category_name,
            'created_at'=> $this->created_at,
            'updated_at'=>$this->updated_at,
            'images' => $this->images,

            // 'product_type_id'=>new ProductTypeResource(ProductType::find($this->product_type_id)->pluck("name")),
            // 'category_id'=>new CategoryResource(Category::find($this->category_id)->pluck("category_name")),
       ];

       return $data;
    }
}
