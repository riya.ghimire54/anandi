<?php

namespace App\Http\Resources\Slider;

use Illuminate\Http\Resources\Json\JsonResource;

class SliderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data =[
            'id'=>$this->id,
            'caption'=>isset($this->caption)?$this->caption:"",
            'created_at'=> $this->created_at,
            'updated_at'=>$this->updated_at,
            'image' => isset($this->image)? env("APP_URL")."/image/sliders/".$this->name."/".$this->image:"",
        ];
        return $data;
    }
}
