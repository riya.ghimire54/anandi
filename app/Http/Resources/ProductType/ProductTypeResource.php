<?php

namespace App\Http\Resources\ProductType;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data =[
            'id'=>$this->id,
             'name'=>$this->name,
             'description'=>$this->description,
             'category_id'=>$this->category_id,
            'created_at'=> $this->created_at,
            'updated_at'=>$this->updated_at,
            'image' => isset($this->image)? env("APP_URL")."/image/product_type/".$this->name."/".$this->image:"",
        ];
        return $data;
    }
}
