<?php

namespace App\Http\Resources\About;

use Illuminate\Http\Resources\Json\JsonResource;

class AboutResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data =[
        'id'=>$this->id,
        'title'=>$this->title,
        'description'=>$this->description,
        'created_at'=> $this->created_at,
        'updated_at'=>$this->updated_at,
        'image' =>env("APP_URL")."/image/about/".$this->image,
        ];
    
    return $data;   
    }
}
