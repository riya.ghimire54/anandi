<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data =[
            'id'=>$this->id,
            'title'=>$this->title,
            'name'=>$this->name,
            'contact_number'=>$this->contact_number,
            'address'=>$this->address,
            'email'=>$this->email,
            // 'email_verified'=>isset($this->email_verified)?$this->email_verified:"",
            // 'is_admin'=>$this->is_admin,
            'created_at'=> $this->created_at,
            'updated_at'=>$this->updated_at,
            'image' => isset($this->image)? env("APP_URL")."/image/users/".$this->name."/".$this->image:"",
        ];
        return $data;
    }
}
