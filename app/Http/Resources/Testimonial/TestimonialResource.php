<?php

namespace App\Http\Resources\Testimonial;

use Illuminate\Http\Resources\Json\JsonResource;

class TestimonialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data =[
            'id'=>$this->id,
             'name'=>$this->name,
             'description'=>$this->description,
            'designation'=>isset($this->designation)?$this->designation:"",
            'created_at'=> $this->created_at,
            'updated_at'=>$this->updated_at,
            'image' => isset($this->image)? env("APP_URL")."/image/testimonials/".$this->name."/".$this->image:"",
        ];
        return $data;
    }
}
