<?php

namespace App\Http\Resources\Setting;

use Illuminate\Http\Resources\Json\JsonResource;

class SettingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data =[
            'id'=>$this->id,
            'business_name'=>$this->business_name,
            'slogan'=>$this->slogan,
            'location_address'=>$this->location_address,
            'contact_info1'=>$this->contact_info1,
            'contact_info2'=>isset($this->contact_info2)?$this->contact_info2:"",
            'email1'=>$this->email1,
            'email2'=>isset($this->email2)?$this->email2:"",
            'copyright_info'=>$this->copyright_info,
            'delivery_details'=>$this->delivery_details,
            'privacy_policy'=>$this->privacy_policy,
            'term_condition'=>$this->term_condition,
            'facebook'=>isset($this->facebook)?$this->facebook:"",
            'twitter'=>isset($this->twitter)?$this->twitter:"",
            'instagram'=>isset($this->instagram)?$this->instagram:"",
            'tiktok'=>isset($this->tiktok)?$this->tiktok:"",

            'created_at'=> $this->created_at,
            'updated_at'=>$this->updated_at,
            'image' =>env("APP_URL")."/image/setting/".$this->image,
            ];
        
        return $data;
    }
}
