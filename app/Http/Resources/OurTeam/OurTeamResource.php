<?php

namespace App\Http\Resources\OurTeam;

use Illuminate\Http\Resources\Json\JsonResource;

class OurTeamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data =[
            'id'=>$this->id,
            'name'=>$this->name,
            'designation'=>$this->designation,
            'facebook'=>isset($this->facebook)?$this->facebook:"",
            'twitter'=>isset($this->twitter)?$this->twitter:"",
            'linkedin'=>isset($this->linkedin)?$this->linkedin:"",
            'instagram'=>isset($this->instagram)?$this->instagram:"",
            'email'=>$this->email,
            'created_at'=> $this->created_at,
            'updated_at'=>$this->updated_at,
            'image' =>  isset($this->image)? env("APP_URL")."/image/ourteams/".$this->name."/".$this->image:"",
            ];
        
        return $data; 
    }
}
