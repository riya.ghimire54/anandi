<?php

use App\Http\Controllers\backend\AboutController;
use App\Http\Controllers\backend\AdministratorController;
use App\Http\Controllers\backend\BlogController;
use App\Http\Controllers\backend\BtagController; 
use App\Http\Controllers\backend\CartController;
use App\Http\Controllers\backend\CategoryController;
use App\Http\Controllers\backend\ContactController;
use App\Http\Controllers\backend\EmailVerificationController;
use App\Http\Controllers\backend\FaqController;
use App\Http\Controllers\backend\JewellryTypeController;
use App\Http\Controllers\backend\OccasionController;
use App\Http\Controllers\backend\OrderController;
use App\Http\Controllers\backend\OurTeamController;
use App\Http\Controllers\backend\ProductController;
use App\Http\Controllers\backend\ProductTypeController;
use App\Http\Controllers\backend\PurityController;
use App\Http\Controllers\backend\SettingController;
use App\Http\Controllers\backend\SliderController;
use App\Http\Controllers\backend\SubscriptionController;
use App\Http\Controllers\backend\TestimonialController;
use App\Http\Controllers\backend\WishlistController;
use App\Http\Controllers\HomeController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Tests\Feature\Auth\EmailVerificationTest;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::post('/login', "Auth\AuthenticatedSessionController@store");

Route::middleware(['auth:sanctum'])->group(function () {
    Route::middleware(['admin'])->group(function () {
        Route::post('/administrator/register',[AdministratorController::class,'store'])->name('register.store');
        Route::put('/administrator/update/{id}',[AdministratorController::class,'update'])->name('register.update'); 
        Route::delete('/administrator/delete/{id}',[AdministratorController::class,'destroy'])->name('register.delete');   

        Route::apiResource('categories',CategoryController::class)->except("index","show");
        Route::apiResource('product_types',ProductTypeController::class)->except("index","show");
        Route::apiResource('occasions',OccasionController::class)->except("index","show");
        Route::apiResource('abouts',AboutController::class)->except("index","show");
        Route::apiResource('faqs',FaqController::class)->except("index","show");
        Route::apiResource('testimonials',TestimonialController::class)->except("index","show");
        Route::apiResource('purities',PurityController::class)->except("index","show");
        Route::apiResource('jewellry_types',JewellryTypeController::class)->except("index","show");
        Route::apiResource('blogs',BlogController::class)->except("index","show");
        Route::apiResource('btags',BtagController::class)->except("index","show");
        Route::apiResource('products',ProductController::class)->except("index","show");
        Route::apiResource('sliders',SliderController::class)->except("index","show");
        Route::post('/products/images', [ProductController::class, 'image']);
        Route::apiResource('orders',OrderController::class)->except("index","show");
        Route::post('settings',[SettingController::class,'update']);  
        Route::apiResource('our_teams',OurTeamController::class)->except("index","show");
        Route::get('subscription',[SubscriptionController::class,'index']);
        Route::apiResource('contact',ContactController::class)->except("store", "update");
        Route::post('contact/reply/{id}',[ContactController::class,'reply']);
    });
    
    Route::apiResource('wishlists',WishlistController::class);
    Route::apiResource('carts',CartController::class);
    Route::apiResource('categories',CategoryController::class)->only("index","show");
    Route::apiResource('categories',CategoryController::class)->only("index","show");
    Route::apiResource('product_types',ProductTypeController::class)->only("index","show");
    Route::apiResource('occasions',OccasionController::class)->only("index","show");
    Route::apiResource('abouts',AboutController::class)->only("index","show");
    Route::apiResource('faqs',FaqController::class)->only("index","show");
    Route::apiResource('testimonials',TestimonialController::class)->only("index","show");
    Route::apiResource('purities',PurityController::class)->only("index","show");
    Route::apiResource('jewellry_types',JewellryTypeController::class)->only("index","show");
    Route::apiResource('blogs',BlogController::class)->only("index","show");
    Route::apiResource('btags',BtagController::class)->only("index","show");
    Route::apiResource('products',ProductController::class)->only("index","show");
    Route::apiResource('sliders',SliderController::class)->only("index","show");
    Route::post('/products/images', [ProductController::class, 'image']);
    Route::apiResource('orders',OrderController::class)->only("index","show");
    Route::get('setting/{id}',[SettingController::class,'show']);  
    Route::post('subscriptions',[SubscriptionController::class,'store']);
    Route::get('our_teams',[OurTeamController::class,'index']);
    Route::post('contacts',[ContactController::class,'store']);
    Route::get('sliders',[HomeController::class,'index']);
});
require __DIR__.'/auth.php';
route::get('email/verify/{id}/{hash}',[EmailVerificationController::class,'verify'])->middleware(['signed','throttle:6,1'])->name('verification.verify');
Route::get('email/resend',[EmailVerificationController::class,'resend'])->name('verification.resend');