<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('business_name');
            $table->string('slogan');
            $table->string('image');
            $table->string('location_address');
            $table->string('contact_info1');
            $table->string('contact_info2')->nullable();
            $table->string('email1');
            $table->string('email2')->nullable();
            $table->string('copyright_info');
            $table->string('delivery_details');
            $table->string('privacy_policy');
            $table->string('term_condition');
            $table->string('facebook')->nullable();
            $table->string('instagram')->nullable();
            $table->string('twitter')->nullable();
            $table->string('tiktok')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
};
