<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('image_featured')->nullable();
            $table->string('description');
            $table->integer('stock');
            $table->float('discount')->nullable();
            $table->integer('price');
            $table->string('tag');
            $table->string('gender');
            $table->unsignedInteger('purity_id');
            $table->unsignedInteger('product_type_id');
            $table->unsignedInteger('category_id');
            $table->string('size')->nullable();
            $table->boolean('featured')->default(0);
            $table->timestamps();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
