<?php

namespace Database\Seeders;

use App\Models\Product;
use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        Product::create([
            'name' => $faker->word,
            'image_featured' => 'image_featured.png',
            "description" => $faker->sentence,
            "stock" => "15",
            "discount" => "0.1",
            "price" => "1500",
            "tag" => $faker->word,
            "size" => $faker->sentence,
            "gender" => $faker->word,
            "purity_id" => "2",
            "product_type_id" => "1",
            "Category_id" => "1",
            "featured"=>"our featured",

        ]);
    }
}
