<?php

namespace Database\Seeders;

use App\Models\ProductType;
use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        ProductType::create([
            "name" => $faker->word,
            "image" => "image.png",
            "description" => $faker->sentence,
            "category_id" => "5"
        ]);
    }
}
