<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(AboutsTableSeeder::class);
        $this->call(BlogsTableSeeder::class);
        $this->call(FaqsTableSeeder::class);
        $this->call(JewellryTypesTableSeeder::class);
        $this->call(OccasionsTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(ProductTypesTableSeeder::class);
        $this->call(PuritiesTableSeeder::class);
        $this->call(TestimonialsTableSeeder::class);
        $this->call(BtagsTableSeeder::class);

    }
}
