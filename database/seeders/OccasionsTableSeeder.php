<?php

namespace Database\Seeders;

use App\Models\Occasion;
use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class OccasionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        Occasion::insert([
            [
                "occasion_name" => $faker->word,
                "description" => $faker->sentence
            ],
            [
                "occasion_name" => $faker->word,
                "description" => $faker->sentence
            ]
        ]);
    }
}
