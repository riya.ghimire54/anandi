<?php

namespace Database\Seeders;

use App\Models\Blog;
use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Date;

class BlogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        Blog::create([
            "title" => $word = $faker->word,
            "slug" => str_slug($word),
            "btag_id" => 5,
            "description" => $faker->sentence,
            "excerpt" => $faker->sentence,
            "publishedDate" => Carbon::today(),
        ]);
    }
}
