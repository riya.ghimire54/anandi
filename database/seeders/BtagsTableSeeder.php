<?php

namespace Database\Seeders;

use App\Models\Btag;
use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BtagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Btag::create([
            'title' => $word = Factory::create()->word,
            'slug' => str_slug($word)
        ]);
    }
}
