<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
            'title' => $title =  "Ms",
            "name"=> $name = 'Riya',
            "email"=> 'riya@gmail.com',
            'contact_number' => "98471151585",
            "password"=> bcrypt('1234riya'),
            "is_admin" => 0,
            "address"=>'chitwan'
            ],
            [
                'title' => "Ms",
            "name"=> 'Admin',
            "email"=> 'admin@gmail.com',
            'contact_number' => "98471151585",
            "password"=> bcrypt('1234admin'),
            "is_admin" => 1,
            "address"=>'kathamandu'
            ]
        ]);
    }
}
