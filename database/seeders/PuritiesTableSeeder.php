<?php

namespace Database\Seeders;

use App\Models\Purity;
use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PuritiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        Purity::insert([
            [
                "purity_type" => $faker->word(),
                "description" => $faker->sentence
            ],
            [
                "purity_type" => $faker->word,
                "description" => $faker->sentence
            ]
        ]);
    }
}
