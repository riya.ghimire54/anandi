<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'business_name' =>'anchor',
            'slogan'=>'best it solution',
            'location_address' =>'kathamandu',
            'contact_info1'=>'984567873',
            'email1'=>'riya@gmail.com',
            'copyright_info'=>'right is reserved',
            'delivery_details'=>'nepal',
            'privacy_policy'=>'this is our privacy.............',
            'term_condition'=>'term and condition',
            


        ]);
    }
}
