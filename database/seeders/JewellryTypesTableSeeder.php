<?php

namespace Database\Seeders;

use App\Models\JewellryType;
use Faker\Factory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class JewellryTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        JewellryType::insert([
            [
                "jewellry_type" => $faker->word(),
                "description" => $faker->sentence
            ],
            [
                "jewellry_type" => $faker->word(),
                "description" => $faker->sentence
            ]
        ]);
    }
}
